#include "proto\hand.h"

Hand::Hand() {

}

Hand::~Hand() {

}

vector <Card> Hand::getVectorMao() {
	return m_mao;
}

void Hand::adicionarCarta(Card carta) {
	m_mao.push_back(carta);
}

//To Do
unsigned int Hand::pontuacao(bool soVisiveis = false) {
	unsigned int score = 0, aceCount = 0;
	for(int i = 0; i < m_mao.size(); i++) {
		if(m_mao[i].isVisivel() || !soVisiveis){
			switch(m_mao[i].getNumero())
		    {
		      case 2: score += 2;
		      break;
		      case 3: score += 3;
		      break;
		      case 4: score += 4;
		      break;
		      case 5: score += 5;
		      break;
		      case 6: score += 6;
		      break;
		      case 7: score += 7;
		      break;
		      case 8: score += 8;
		      break;
		      case 9: score += 9;
		      break;
		      case 10: score += 10;
		      break;
		      case 11: score += 10;
		      break;
		      case 12: score += 10;
		      break;
		      case 13: score += 10;
		      break;
		      case 14: score+= 1;
		      		   aceCount++;
		      break;
		    }
		}
	}

	if (aceCount > 0 && score < 12) {
		score += 10;	
	}

	return score;
}

void Hand::nomesCartas() {
	bool comma = false;
	for(int i = 0; i < m_mao.size(); i++) {
			if (comma)
				cout << ", ";
			comma = true;
		if(m_mao[i].isVisivel())
			cout << m_mao[i].nomeCarta();
		else 
			cout << "Carta Virada para Baixo";
	}
	cout << endl;
}

void Hand::setCartasVisiveis() {
	for(int i = 0; i < m_mao.size(); i++) {
		m_mao[i].setVisibilidade(true);
	}
}

void Hand::reset() {
	m_mao.clear();
}
#ifndef GAME_H
#define GAME_H

class Game
{
public:
	Game();
	~Game();
	void adicionarJogador(Player_info);
	void novaPartida();
	void inicioRonda(); 
	void fimRonda();
	//void novaRonda();
	void mostrarMesa();
	void mostrarSaldos(bool);
	void darCartasIniciais();

	void titulo_fimdaronda();
	void titulo_menu();
	void titulo_blackjack();
	void titulo_regras();
	void titulo_inserir();

	void fim();

	void atribuirCartas(unsigned int, unsigned int);
	void eliminarJogadorDaPartida(unsigned int); //valor apostado = 0; removido do vector; isPlaying = False
	char perguntarAccao(unsigned int);  //perde apostado; isPlaying = false; estado = false
	void jogadorDesistir(unsigned int);  //perde apostado; isPlaying = false; estado = false
	void jogadorStand(unsigned int);  
	void jogadorHit(unsigned int);  
	void jogadorDouble(unsigned int);  
	bool verificarSeRebentou(unsigned int); //pont > 21

	bool verificarBlackjack();
private:
	vector <Player_info> m_jogadores_info;
	vector <int> m_id_jogadores; //Id dos jogadores que se encontram em jogo
	int m_dealer_id;
	Match m_partida; //Corresponde à ultima
	string m_cabecalho;
};

#endif
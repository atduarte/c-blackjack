#ifndef CARD_H
#define CARD_H

class Card
{
public:
	Card(int, int, bool);
	~Card();
	string nomeCarta();
	int getNaipe();
	int getNumero();
	bool isVisivel();
	void setVisibilidade(bool);
private:
	int m_naipe;
	//0 - Espadas; 1 - Paus; 2 - Copas; 3 - Ouros
	int m_numero;
	// 2-10; 11 - Dama; 12 - Valete; 13 - Rei; 14 - Ás
	bool m_visibilidade;
};

#endif
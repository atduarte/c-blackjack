#ifndef DECK_H
#define DECK_H

class Deck
{
public:
	Deck();
	~Deck();
	void baralhar();
	vector <Card> buscarCartas(int);
	unsigned int nCartas();
	void reset();
private:
	vector <Card> m_baralho;
};

#endif
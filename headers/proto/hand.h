#ifndef HAND_H
#define HAND_H

class Hand
{
public:
	Hand();
	~Hand();
	vector <Card> getVectorMao();
	void adicionarCarta(Card);
	unsigned int pontuacao(bool);
	void nomesCartas();
	void setCartasVisiveis();
	void reset();
private:
	vector <Card> m_mao;
};

#endif
#ifndef Player_INFO_H
#define Player_INFO_H

class Player_info
{
public:
	Player_info(string, double, bool);
	~Player_info();

	string getNome();
	double getSaldo();
	bool getEstado();
	bool isDoubled();
	double getValorApostado();

	void adicionaSaldo(double);
	void subtraiSaldo(double);
	void desistir();
	void setValorApostado(double);
	void setDoubled(bool);
	void setEstado(bool);

	//Saldo =+ Valor apostado * double;
	void premiar(double);

	void reset();

	Hand m_mao;
private:
	string m_nome;
	double m_saldo;
	bool m_estado; //se desistiu ou nao

	double m_valor_apostado;
	bool m_isDoubled;
};

#endif
#include "proto\player_info.h"

Player_info::Player_info(string nome, double saldo, bool estado = true) {
	m_nome = nome;
	m_saldo = saldo;
	m_estado = estado;
	m_valor_apostado = 0;
	m_isDoubled = false;
}

Player_info::~Player_info() {

}




string Player_info::getNome() {
	return m_nome;
}

double Player_info::getSaldo() {
	return m_saldo;
}

bool Player_info::getEstado() {
	return m_estado;
}

bool Player_info::isDoubled() {
	return m_isDoubled;
}

double Player_info::getValorApostado() {
	return m_valor_apostado;
}



void Player_info::adicionaSaldo(double valor) {
	m_saldo += valor;
}

void Player_info::subtraiSaldo(double valor) {
	m_saldo -= valor;
}

void Player_info::desistir() {
	m_estado = false;
}

void Player_info::setValorApostado(double valor_apostado) {
	m_valor_apostado = valor_apostado;
}
void Player_info::setDoubled(bool isDoubled) {
	m_isDoubled = isDoubled;
}
void Player_info::setEstado(bool estado) {
	m_estado = estado;
}




void Player_info::premiar(double multiplicador = 1) {
	m_saldo += m_valor_apostado * multiplicador;
	m_valor_apostado = 0;
}

void Player_info::reset() {
	m_valor_apostado = 0;
	m_isDoubled = false;
	m_mao.reset();
}
#include "proto\card.h"

Card::Card(int naipe, int numero, bool visibilidade = true)
{
  m_naipe=naipe;
  m_numero=numero;
  m_visibilidade = visibilidade;
}

Card::~Card()
{
}

int Card::getNaipe()
{
  return m_naipe;
}

int Card::getNumero()
{
  return m_numero;
}

bool Card::isVisivel()
{
  return m_visibilidade;
}

void Card::setVisibilidade(bool visibilidade)
{
  m_visibilidade = visibilidade;
}

string Card::nomeCarta()
{
   string nome;
   switch(m_numero)
   {
      case 2: nome = "Dois ";
      break;
      case 3: nome = "Tres ";
      break;
      case 4: nome = "Quatro ";
      break;
      case 5: nome = "Cinco ";
      break;
      case 6: nome = "Seis ";
      break;
      case 7: nome = "Sete ";
      break;
      case 8: nome = "Oito ";
      break;
      case 9: nome = "Nove ";
      break;
      case 10: nome = "Dez ";
      break;
      case 11: nome = "Dama ";
      break;
      case 12: nome = "Valete ";
      break;
      case 13: nome = "Rei ";
      break;
      case 14: nome = "As ";
      break;
  }
  nome += "de ";
  switch(m_naipe)
  {
      case 0: nome += "Espadas";
      break;
      case 1: nome += "Paus";
      break;
      case 2: nome += "Copas";
      break;
      case 3: nome += "Ouros";
      break;
  }     
  return nome;
}
#include "proto\game.h"

Game::Game() {
	m_cabecalho = "";
}

Game::~Game() {

}

void Game::adicionarJogador(Player_info player_info) {
	m_jogadores_info.push_back(player_info);
}

void Game::novaPartida() {

	m_id_jogadores.clear();

	int j = 0;
	//Buscar IDs dos Jogadores que ainda estão em jogo e selecionar 4
	for(int i=0; i < m_jogadores_info.size(); i++) {

		if(m_jogadores_info[i].getEstado() && m_jogadores_info[i].getSaldo() > 0) {

			//Reset Jogador
			m_jogadores_info[i].reset();
			m_partida.reset();

			//Definir Dealer
			if(j == 0) {
				m_dealer_id = i;
			}

			//Definir como jogador
			if(j < 4) {
				m_id_jogadores.push_back(i);
				//Definir valor apostado igual para todos
				//m_jogadores_info[i].setValorApostado(valor_aposta);
				//m_jogadores_info[i].subtraiSaldo(valor_aposta);
			}

			//Contador de jogadores. Max: 4
			j++;
		}
	}

	//Perguntar quanto cada um quer apostar
	for(int i = 1; i < m_id_jogadores.size(); i++) {
		system("cls");
		cout << m_cabecalho;
		centrado("                       _            ");
		centrado("  __ _ _ __   ___  ___| |_ __ _ ___ ");
		centrado(" / _` | '_ \\ / _ \\/ __| __/ _` / __|");
		centrado("| (_| | |_) | (_) \\__ \\ || (_| \\__ \\");
		centrado(" \\__,_| .__/ \\___/|___/\\__\\__,_|___/");
		centrado("      |_|                           ");
		cout << endl;

		centradoString("Ola, " + m_jogadores_info[m_id_jogadores[i]].getNome());

		//Mostrar os valores apostados pelos outros
		double valor_aposta = getFloat( 0, 
										m_jogadores_info[m_id_jogadores[i]].getSaldo(),
										"Que valor pretende apostar?",
										"Valor introduzido incorrectamente. Tente novamente.",
										"");
		
		m_jogadores_info[m_id_jogadores[i]].subtraiSaldo(valor_aposta);
		m_jogadores_info[m_id_jogadores[i]].setValorApostado(valor_aposta);


	}

	//Distribuir as duas cartas
	this->darCartasIniciais();

	this->inicioRonda();
}

void Game::darCartasIniciais() {

	for(int i = 0; i < m_id_jogadores.size(); i++) {

		vector <Card> cartasTemp = m_partida.m_baralho.buscarCartas(2);

		//Se for o Dealer -> Segunda carta não é visivel
		if(m_id_jogadores[i] == m_dealer_id)
			cartasTemp[1].setVisibilidade(false);

		m_jogadores_info[m_id_jogadores[i]].m_mao.adicionarCarta(cartasTemp[0]);
		m_jogadores_info[m_id_jogadores[i]].m_mao.adicionarCarta(cartasTemp[1]);

	}
}

void Game::atribuirCartas(unsigned int ID, unsigned int n) {
	vector <Card> cartasTemp = m_partida.m_baralho.buscarCartas(n);

	for (int i = 0; i < n; ++i)
	{
		m_jogadores_info[ID].m_mao.adicionarCarta(cartasTemp[i]);
	}
}

void Game::fimRonda() {
	//Buscar ID Jogadores prontos e que estavam a jogar
	vector <unsigned int> jogadores_prontos;
	for (int i = 0; i < m_id_jogadores.size(); ++i)
	{
		if(m_jogadores_info[m_id_jogadores[i]].getSaldo() > 0 && m_dealer_id != i)
			jogadores_prontos.push_back(m_id_jogadores[i]);
	}

	//Caso haja jogadores prontos
	if(jogadores_prontos.size() > 0) {
		//perguntar as desistencias
		system("cls");
		centrado("     _           _     _   _      ");
		centrado("  __| | ___  ___(_)___| |_(_)_ __ ");
		centrado(" / _` |/ _ \\/ __| / __| __| | '__|");
		centrado("| (_| |  __/\\__ \\ \\__ \\ |_| | |   ");
		centrado(" \\__,_|\\___||___/_|___/\\__|_|_|   ");
		cout << endl;

		for (int i = 0; i < jogadores_prontos.size(); ++i)
		{
			char op = 'o';
			do{
				centradoString("Ola, " + m_jogadores_info[jogadores_prontos[i]].getNome());
				centrado("Pretendes desistir? (Y/N) ", false);
				getline(cin, buffer);
				if(buffer.size() == 1)
					op = toupper(buffer[0]);
				switch (op) {
					case 'Y': jogadorDesistir(jogadores_prontos[i]);
							  break;
					case 'N': break;
					default: op = 'o';
				}
			} while(op == 'o');
		}

		//Contar os jogadores ainda prontos
		jogadores_prontos.clear();
		unsigned int j = 0;
		for (int i = 0; i < m_jogadores_info.size(); ++i)
		{
			if(m_jogadores_info[i].getSaldo() > 0 && m_dealer_id != i && m_jogadores_info[i].getEstado())
				j++;
		}
		if(j > 0) {
			this->novaPartida();
			return;
		}
	} 

	this->fim();
}

void Game::eliminarJogadorDaPartida(unsigned int ID) {
	m_jogadores_info[ID].setValorApostado(0);

	//Remover do vector de Jogadores que estão a jogar
	m_id_jogadores.erase(std::remove(m_id_jogadores.begin(), m_id_jogadores.end(), ID), m_id_jogadores.end());
}

void Game::jogadorDesistir(unsigned int ID) {
	m_jogadores_info[ID].setEstado(false);

	this->eliminarJogadorDaPartida(ID);
}

char Game::perguntarAccao(unsigned int ID) {

	//Limpar ecrã
	system("cls");
	cout << m_cabecalho;
	this->mostrarMesa();

	centrado("           _                        ");
	centrado("  __ _    (_) ___   __ _  __ _ _ __ ");
	centrado(" / _` |   | |/ _ \\ / _` |/ _` | '__|");
	centrado("| (_| |   | | (_) | (_| | (_| | |   ");
	centrado(" \\__,_|  _/ |\\___/ \\__, |\\__,_|_|   ");
	centrado("        |__/       |___/            ");
	cout << endl;

	//Inicialização
	string buffer;
	char op;
	bool repeat = false;
	centradoString("Ola, " + m_jogadores_info[ID].getNome());
		
	do {
		if(m_jogadores_info[ID].getSaldo() >= m_jogadores_info[ID].getValorApostado() && m_jogadores_info[ID].m_mao.pontuacao() > 8 && m_jogadores_info[ID].m_mao.pontuacao() < 12)
			centrado("Pretende fazer Stand (S), Hit (H), Double (D) ou Desistir (Q)?", false);
		else
			centrado("Pretende fazer Stand (S), Hit (H) ou Desistir (Q)?", false);

		getline(cin,buffer);
		op = buffer[0];
		op = toupper(op);

		if(op == 'S' || op == 'H' || op == 'Q' || 
		   op == 'D' && m_jogadores_info[ID].getSaldo() >= m_jogadores_info[ID].getValorApostado())
		{
			return op;
			repeat = false;
		} else {
			centrado("Valor invalido. Tente Novamente.");
			repeat = true;
		}

	} while (repeat = true);

	return 'a'; // O mesmo warning de sempre ....
}

void Game::jogadorStand(unsigned int ID) {
	
}

void Game::jogadorHit(unsigned int ID) {
	this->atribuirCartas(ID, 1);
	centradoString(m_jogadores_info[ID].m_mao.getVectorMao()[m_jogadores_info[ID].m_mao.getVectorMao().size()-1].nomeCarta() + " | Pontuacao: " + toString(m_jogadores_info[ID].m_mao.pontuacao()));
}

void Game::jogadorDouble(unsigned int ID) {
	this->atribuirCartas(ID, 1);
	double valor_apostado = m_jogadores_info[ID].getValorApostado();
	m_jogadores_info[ID].setValorApostado(valor_apostado*2);
	m_jogadores_info[ID].subtraiSaldo(valor_apostado);
	m_jogadores_info[ID].setDoubled(true);
	centradoString(m_jogadores_info[ID].m_mao.getVectorMao()[m_jogadores_info[ID].m_mao.getVectorMao().size()-1].nomeCarta() + " | Pontuacao: " + toString(m_jogadores_info[ID].m_mao.pontuacao()));
	cout << endl;
}

bool Game::verificarSeRebentou(unsigned int ID) {
	if(m_jogadores_info[ID].m_mao.pontuacao() > 21) {
		//rebentou
		return true;
	} else {
		//não rebentou
		return false;
	}
}


bool Game::verificarBlackjack() {
	vector <int> jogadores_id_com_blackjack, jogadores_id_sem_blackjack;

	//Get IDs que têm BlackJack
	for(int i = 0; i < m_id_jogadores.size(); i++) {
		if(m_jogadores_info[m_id_jogadores[i]].m_mao.pontuacao() == 21) {
			jogadores_id_com_blackjack.push_back(m_id_jogadores[i]);
		} else {
			jogadores_id_sem_blackjack.push_back(m_id_jogadores[i]);
		}
	}

	//Caso haja alguém que tenha feito blackjack
	if(jogadores_id_com_blackjack.size() > 0) {
		m_jogadores_info[m_dealer_id].m_mao.setCartasVisiveis();
		//Caso o dealer seja um deles
		if(m_jogadores_info[m_dealer_id].m_mao.pontuacao() == 21) {
			//Retirar saldo a todos os que não fizeram e o Dealer fica com isso
			for(int i = 0; i < jogadores_id_sem_blackjack.size(); i++) {
				m_jogadores_info[m_dealer_id].adicionaSaldo(m_jogadores_info[jogadores_id_sem_blackjack[i]].getValorApostado());
				m_jogadores_info[jogadores_id_sem_blackjack[i]].setValorApostado(0);
			}
			//Devolver valor apostado a quem tbm fez blackjack
			for(int i = 0; i < jogadores_id_com_blackjack.size(); i++) {
				m_jogadores_info[jogadores_id_com_blackjack[i]].premiar(1);
			}
		} else {
			//Premiar quem fez blackjack
			for(int i = 0; i < jogadores_id_com_blackjack.size(); i++) {
				m_jogadores_info[jogadores_id_com_blackjack[i]].premiar(1.5);
			}
			//Limpar valores apostados
			for(int i = 0; i < m_jogadores_info.size(); i++) {
				m_jogadores_info[i].setValorApostado(0);
			}
		}

		system("cls");
		cout << m_cabecalho;
		this->titulo_fimdaronda();
		cout << endl;
		this->mostrarSaldos(false);

		// Mostrar que alguém fez
		// BLACKJACK
		buffer = "";
		if(jogadores_id_com_blackjack.size() == 1)
			buffer += "O ";
		else
			buffer += "O ";

		for (int i = 1; i <= jogadores_id_com_blackjack.size(); ++i)
		{
			buffer += m_jogadores_info[jogadores_id_com_blackjack[i-1]].getNome();
			if(i < jogadores_id_com_blackjack.size()-1)
				buffer += ", o ";
			else if (i == jogadores_id_com_blackjack.size() -1)
				buffer += " e o ";

		}

		if(jogadores_id_com_blackjack.size() == 1)
			buffer += " fez Blackjack.";
		else
			buffer += " fizeram Blackjack.";

		centradoString(buffer);

		cout << endl;

		centrado("Prima qualquer tecla para continuar...", false);
		system("pause > nul");
		return true;
	} else {
		return false;
	}
}

void Game::mostrarMesa() {
	centrado(" _ __ ___   ___  ___  __ _ ");
	centrado("| '_ ` _ \\ / _ \\/ __|/ _` |");
	centrado("| | | | | |  __/\\__ \\ (_| |");
	centrado("|_| |_| |_|\\___||___/\\__,_|");
	cout << endl;
	for(int i = 0; i < m_id_jogadores.size(); i++) {
		cout << "- " << m_jogadores_info[m_id_jogadores[i]].getNome() << endl; 
		if(m_id_jogadores[i] != m_dealer_id)
			cout << "Saldo: " << m_jogadores_info[m_id_jogadores[i]].getSaldo() << " - Apostado: " 
			 	 << m_jogadores_info[m_id_jogadores[i]].getValorApostado() << endl;
		cout << "Cartas: ";
		m_jogadores_info[m_id_jogadores[i]].m_mao.nomesCartas();
		cout << "Pontuacao: " << m_jogadores_info[m_id_jogadores[i]].m_mao.pontuacao(true) << endl << endl << endl;
	}
}

void Game::mostrarSaldos(bool mostrarTitulo = true) {
	if(mostrarTitulo) {
		centrado("           _     _           ");
		centrado(" ___  __ _| | __| | ___  ___ ");
		centrado("/ __|/ _` | |/ _` |/ _ \\/ __|");
		centrado("\\__ \\ (_| | | (_| | (_) \\__ \\");
		centrado("|___/\\__,_|_|\\__,_|\\___/|___/");
		cout << endl;
	}
	for(int i = 0; i < m_id_jogadores.size(); i++) {
		cout << "- " << m_jogadores_info[m_id_jogadores[i]].getNome() << endl; 
		if(m_id_jogadores[i] != m_dealer_id)
			cout << "Novo saldo: " << m_jogadores_info[m_id_jogadores[i]].getSaldo() << endl;
		cout << "Cartas: ";
		m_jogadores_info[m_id_jogadores[i]].m_mao.nomesCartas();
		cout << "Pontuacao Final: " << m_jogadores_info[m_id_jogadores[i]].m_mao.pontuacao() << endl << endl << endl;
	}
}

void Game::inicioRonda() {

	if (this->verificarBlackjack()) {
		this->fimRonda();
		return;
	}

	system("cls");
	cout << m_cabecalho;
	this->mostrarMesa();
	
	for (int i = 1; i < m_id_jogadores.size(); ++i)
	{
		bool repeat = false;
		do {
			char op = this->perguntarAccao(m_id_jogadores[i]);
			repeat = false;
			switch(op) {
			case 'S':	break;
			case 'H':	this->jogadorHit(m_id_jogadores[i]);
						if(verificarSeRebentou(m_id_jogadores[i])) {
							centrado("Rebentou!");
						} else if (m_jogadores_info[m_id_jogadores[i]].m_mao.pontuacao() == 21){
							centrado("Fizeste Blackjack!");
						} else {
							repeat = true;
							cout << endl;
							centrado("Prima qualquer tecla para decidir o proximo passo...", false);
							system("pause > nul");
						}
						break;
			case 'D':	this->jogadorDouble(m_id_jogadores[i]);
						break;
			case 'Q':	this->jogadorDesistir(m_id_jogadores[i]);
						i--;
						break;
			}
		} while (repeat == true);

		cout << endl;
		centrado("Prima qualquer tecla para continuar...",false);
		system("pause > nul");
	}

	//
	//A vez do DEALER
	//

	//Pôr as cartas do Dealer visiveis
	m_jogadores_info[m_dealer_id].m_mao.setCartasVisiveis(); 

	system("cls");
	cout << m_cabecalho;
	this->mostrarMesa();


	while(m_jogadores_info[m_dealer_id].m_mao.pontuacao() < 17) {
		cout << "O Dealer fez Hit." << endl;
		this->jogadorHit(m_dealer_id);
		cout << endl;
	}
	if(m_jogadores_info[m_dealer_id].m_mao.pontuacao() > 21)
		cout << "O Dealer rebentou." << endl;
	else
		cout << "O Dealer fez Stand." << endl;

	//
	//Distribuir Prémios
	//

	//Verificar se alguém tem blackjack e distribuir prémios para esse caso
	//Comparar pontuações com o Dealer e premiar devidamente
	for (int i = 1; i < m_id_jogadores.size(); ++i)
	{
		//Se jogador rebentar ou tiver menos pontos que o dealer
		if(m_jogadores_info[m_id_jogadores[i]].m_mao.pontuacao() > 21 || 
		   (m_jogadores_info[m_dealer_id].m_mao.pontuacao() > m_jogadores_info[m_id_jogadores[i]].m_mao.pontuacao() && m_jogadores_info[m_dealer_id].m_mao.pontuacao() <= 21)) 
		{
			m_jogadores_info[m_dealer_id].adicionaSaldo( m_jogadores_info[m_id_jogadores[i]].getValorApostado() );
			m_jogadores_info[m_id_jogadores[i]].premiar(0);
		//Se o dealer rebentar ou se o jogador tiver mais ou igual numero de pontos que o dealer
		} else if (m_jogadores_info[m_dealer_id].m_mao.pontuacao() > 21 ||
				   m_jogadores_info[m_dealer_id].m_mao.pontuacao() <= m_jogadores_info[m_id_jogadores[i]].m_mao.pontuacao()) 
		{
			if(m_jogadores_info[m_id_jogadores[i]].isDoubled())
				m_jogadores_info[m_id_jogadores[i]].premiar(2);
			else
				m_jogadores_info[m_id_jogadores[i]].premiar(1);
		}
	}

		//Mostras Saldos no final da Ronda
		system("cls");
		cout << m_cabecalho;
		this->titulo_fimdaronda();
		this->mostrarSaldos(false);
		centrado("Prima qualquer tecla para continuar...",false);
		system("pause > nul");
	

	this->fimRonda();
}

void Game::fim() {
	system("cls");
	centrado("  __ _           ");
	centrado(" / _(_)_ __ ___  ");
	centrado("| |_| | '_ ` _ \\ ");
	centrado("|  _| | | | | | |");
	centrado("|_| |_|_| |_| |_|");
	cout << endl;
	centrado("Prima qualquer tecla para continuar...",false);
	system("pause > nul");
}

void Game::titulo_menu() {
	centrado(" _ __ ___   ___ _ __  _   _ ");
    centrado("| '_ ` _ \\ / _ \\ '_ \\| | | |");
    centrado("| | | | | |  __/ | | | |_| |");
    centrado("|_| |_| |_|\\___|_| |_|\\__,_|");
    cout << endl;
}

void Game::titulo_fimdaronda() {
	centrado("  __ _                 _                             _       ");
	centrado(" / _(_)_ __ ___     __| | __ _   _ __ ___  _ __   __| | __ _ ");
	centrado("| |_| | '_ ` _ \\   / _` |/ _` | | '__/ _ \\| '_ \\ / _` |/ _` |");
	centrado("|  _| | | | | | | | (_| | (_| | | | | (_) | | | | (_| | (_| |");
	centrado("|_| |_|_| |_| |_|  \\__,_|\\__,_| |_|  \\___/|_| |_|\\__,_|\\__,_|");
	cout << endl;
}

void Game::titulo_blackjack() {
	centrado(" _     _            _     _            _    ");
    centrado("| |__ | | __ _  ___| | __(_) __ _  ___| | __");
    centrado("| '_ \\| |/ _` |/ __| |/ /| |/ _` |/ __| |/ /");
    centrado("| |_) | | (_| | (__|   < | | (_| | (__|   < ");
    centrado("|_.__/|_|\\__,_|\\___|_|\\_\\/ |\\__,_|\\___|_|\\_\\");
    centrado("                       |__/                 ");
    cout << endl;
    cout << endl;
    centrado("    \4             \6                        \5\5\5   ");
    centrado("   \4\4\4           \6\6\6        \3\3   \3\3       \5\5\5\5\5  ");
    centrado("  \4\4\4\4\4         \6\6\6\6\6      \3\3\3\3 \3\3\3\3      \5\5\5\5\5  ");
    centrado(" \4\4\4\4\4\4\4       \6\6\6\6\6\6\6     \3\3\3\3\3\3\3\3\3     \5\5\5\5\5\5\5 ");
    centrado("\4\4\4\4\4\4\4\4\4     \6\6\6\6\6\6\6\6\6    \3\3\3\3\3\3\3\3\3    \5\5\5\5 \5\5\5\5");
    centrado(" \4\4\4\4\4\4\4      \6\6\6\6\6\6\6\6\6     \3\3\3\3\3\3\3     \5\5\5\5 \5\5\5\5");
    centrado("  \4\4\4\4\4       \6\6\6\6 \6\6\6\6      \3\3\3\3\3       \5\5 \5 \5\5 ");
    centrado("   \4\4\4         \6\6 \6 \6\6        \3\3\3           \5    ");
    centrado("    \4             \6            \3           \5\5\5   ");
    centrado("                 \6\6\6                             ");
    cout << endl;
}

void Game::titulo_regras() {
	centrado(" _ __ ___  __ _ _ __ __ _ ___ ");
	centrado("| '__/ _ \\/ _` | '__/ _` / __|");
	centrado("| | |  __/ (_| | | | (_| \\__ \\");
	centrado("|_|  \\___|\\__, |_|  \\__,_|___/");
	centrado("          |___/               ");
	cout << endl;
}

void Game::titulo_inserir() {
	centrado(" _                     _      ");
	centrado("(_)_ __  ___  ___ _ __(_)_ __ ");
	centrado("| | '_ \\/ __|/ _ \\ '__| | '__|");
	centrado("| | | | \\__ \\  __/ |  | | |   ");
	centrado("|_|_| |_|___/\\___|_|  |_|_|   ");
	cout << endl;
}
#include "proto\deck.h"

Deck::Deck() {
	for(int i = 0; i <= 3; i++) {
		for(int j=2; j<= 14; j++) {
			Card carta(i,j);
			m_baralho.push_back(carta);
		}
	}
}

Deck::~Deck() {
	
}

void Deck::baralhar() {
	srand(time(NULL));
	random_shuffle( m_baralho.begin(), m_baralho.end() );
}

vector <Card> Deck::buscarCartas(int n) {
	baralhar();
	
	vector <Card> valor_de_retorno;

	for(int i = 0; i < n; i++)
		valor_de_retorno.push_back(m_baralho[i]);

	m_baralho.erase (m_baralho.begin(),m_baralho.begin()+n);

	return valor_de_retorno;
}

void Deck::reset() {
	m_baralho.clear();
	for(int i = 0; i <= 3; i++) {
		for(int j=2; j<= 14; j++) {
			Card carta(i,j);
			m_baralho.push_back(carta);
		}
	}
}

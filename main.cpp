#include <cstdlib>
#include <cctype>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <time.h>
#include <algorithm>


using namespace std;
string buffer;

template <class T>
string toString( T argument)
{
       string r;
       stringstream s;

       s << argument;
       r = s.str();

       return r;
}

void centrado(char *s, bool endline = true)
{
   int l=strlen(s);
   int pos=(int)((80-l)/2);
   for(int i=0;i<pos;i++)
    cout<<" ";

   cout << s;
   if(endline)
    cout << endl;
}

void centradoString(string s, bool endline = true)
{
   int l = s.size();
   int pos=(int)((80-l)/2);
   for(int i=0;i<pos;i++)
    cout<<" ";

   cout << s;
   if(endline)
    cout << endl;
}

bool isInteger(const std::string &s)
{
    if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+')))
      return false;

    char * p ;
    strtol(s.c_str(), &p, 10) ;
    return true ;
}

bool isFloat(const string &myString ) {
    std::istringstream iss(myString);
    float f;
    iss >> noskipws >> f;// noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail(); 
}

float getFloat(float min, float max, string message, string error_message, string success_message) {
  bool repeat;
  do {
    repeat = false;
    centradoString(message + " ", false);
    string linebuffer;
    getline(cin, linebuffer);
    if(isFloat(linebuffer)) {
      //Verificar limite inferior
      if(atof(linebuffer.c_str()) < min) {
        centrado("Valor demasiado baixo.");
        cout << endl;
        repeat = true;
      //Verificar limite superior
      } else if (atof(linebuffer.c_str()) > max) {
        centrado("Valor demasiado alto.");
        cout << endl;
        repeat = true;
      //Se estiver dentro dos limites
      } else {
        centradoString(success_message);
        //system("pause > nul");
        return atof(linebuffer.c_str());
      }
    } else {
      centradoString(error_message);
      cout << endl;
      repeat = true;
    }

  } while (repeat);
  
  return 0; //não deveria ser necessário, mas compilador dá aviso
}

#include "headers\card.cpp"
#include "headers\deck.cpp"
#include "headers\hand.cpp"
#include "headers\player_info.cpp"
#include "headers\match.cpp"
#include "headers\game.cpp"

Game blackjack;

void menu_inicio()
{
    cout << endl;

    centrado("Bem-vindo");

    blackjack.titulo_blackjack();

    cout << endl;
    centrado("Comecar o jogo...", false);
    system("PAUSE > nul");
    return;
}

void regras()
{
       system("cls");
       blackjack.titulo_regras();
       cout << "O objetivo do jogo e' conseguir um resultado mais perto de 21 sem ultrapassar" << endl << "este valor." << endl << endl
       << "Para jogar:" << endl << "   Os jogadores colocam as suas apostas em jogo. O Dealer ira entao dar duas "<<endl<<"cartas com a face para cima para cada Jogador e duas para ele mesmo, uma para"<<endl<<"cima e outra para baixo." << endl << endl
       << "Valores das cartas:" << endl << "   Rei, Dama, Valete e 10 cada uma delas valem 10." << endl << "   As vale 1 ou 11, como o jogador desejar." << endl << "   Todas as outras cartas sao contadas de acordo com os valores de face." << endl << endl<<endl;
       centrado("Voltar ao menu...", false);
       system("PAUSE > nul");
}


void inserirManualmente()
{
  system("cls");
  
  blackjack.titulo_inserir();

  string  nomeJogador;
  double saldoJogador;
  int int_nrJog;
  bool repeat;

  do{
    repeat = false;
    centrado("Insercao Manual - Activada");
    centrado("Nao foram encontradas informacoes no ficheiro players.txt");
    cout << endl;
    centrado("Quantos jogadores deseja inserir? ", false);
    getline(cin,buffer);
    if(!isInteger(buffer) || atoi(buffer.c_str()) < 1) {
      centrado("Tente novamente.");
      cout << endl;
      repeat = true;
    } else {
      int_nrJog = atoi(buffer.c_str());
    }
  } while(repeat == true);

  for (int i=0;i<int_nrJog;i++)
  {
    system("cls");

    blackjack.titulo_inserir();

    centrado("Jogador numero ",false);
    cout << i+1 << endl;
    centrado("Insira o nome do Jogador: ",false);
    getline(cin, nomeJogador);

    do {
      repeat = false;
      cout << endl;
      centrado("Insira o saldo do Jogador: ",false);
      getline(cin, buffer);

      if(!isFloat(buffer)) {
        centrado("Tente novamente.");
        cout << endl;
        repeat = true;
      } else {
        saldoJogador = atof(buffer.c_str());
      }
    } while (repeat == true);

    blackjack.adicionarJogador(Player_info(nomeJogador, saldoJogador));
  }

  blackjack.novaPartida();

}

void inserir() {

  ifstream jogadores ("players.txt");

  int i = 0, j = 0;
  if(jogadores) {
    vector <string> nomes;
    vector <double> saldos;
    while(!jogadores.eof()) {
      getline(jogadores, buffer);
      if(i%2 == 0) {
        nomes.push_back(buffer);
      } else {
        saldos.push_back(atof(buffer.c_str()));
      }      
      i++;
    }
    for (int i = 0; i < saldos.size(); ++i)
    {
      blackjack.adicionarJogador(Player_info(nomes[i], saldos[i]));
      j++;
    }
  }

  if(j == 0)
    inserirManualmente();
  else
    blackjack.novaPartida();
}

int main()
{
	blackjack.adicionarJogador(Player_info("Dealer", 999999));

  system("TITLE BlackJack - O jogo - Por Andre Duarte e Pedro Santos");
  system("color 2F");

  menu_inicio();
  char op = '0';
  do
  {
      system("cls");
      blackjack.titulo_menu();
      centrado("============================");
      centrado("||                        ||");
      centrado("|| 1 - Iniciar Jogo       ||");
      centrado("|| 2 - Regras             ||");
      centrado("|| 3 - Sair               ||");
      centrado("||                        ||");
      centrado("============================");
      cout << endl;
      centrado("Inserir a sua opcao: ", false);

      getline(cin, buffer);
      if(buffer.size() == 1)
        op = buffer[0];
      switch(op)
      {
                case '1': inserir();
                          op = '3';
                          break;
                case '2': regras();
                          op = '3';
                          break;
      }
  }while(op!='3');


  return EXIT_SUCCESS;
}

